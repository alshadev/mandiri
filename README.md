# Costume Rent - Mandiri

Sebuah aplikasi penyewaan kostum yang memungkinkan untuk mencari sebuah product, memesan sebuah product dan melakukan pembayaran dengan mudah.

Technology Stackk yang digunakan: .NET 8, Entity Framework, Code First, Microservice, xUnit, RabbitMq, SQLite

## Technology

### Architecture
aplikasi ini dibuat dengan desain microservice, untuk saat ini memiliki Transaction Service dan Payment Service.
![Architecture](img/cosrent-arch-2.png)

## Services

### 1. Transaction Service
Memiliki beberapa module seperti Product, Tag dan Order.
- Pengguna dapat melakukan Create, Read, Update dan Delete (CRUD) pada module Product.
- Tag akan terbentuk bersamaan pada saat melakukan Create atau Update Product.
- Pengguna dapat melakukan Create Order, Order yang dibuat memiliki status awal Submitted yang nantinya akan berubah menjadi Paid jika sudah memanggil API untuk melakukan pembayaran pada Payment Service.
- Pengguna dapat melakukan Cancel Order, Status pada sebuah Order yang telah dibuat sebelumnya akan menjadi Cancelled, hanya Order dengan Status Submitted yang dapat dilakukan Cancel.

### 2. Payment Service
Memiliki module untuk melakukan pembayaran berdasarkan Order Id, jika pembayaran bershasil maka service ini akan melakukan publish message yang nantinya akan disubscribe oleh transaction service untuk melakukan update pada Order Status yang awalnya Submitted menjadi Paid.

## Persiapan dan Penggunaan

#### Database
Database yang digunakan adalah SQLite. Untuk database browser bisa menggunakan [DB Browser](https://sqlitebrowser.org/dl/) atau yang lainnya yang dapat dikoneksikan ke SQLite.

#### Menjalankan Program
- Clone atau Download source code.
- Buka `cosrent-mandiri.sln` menggunakan Visual Studio (atau program lainnya yang dapat digunakan untuk menjalankan aplikasi .NET di local masing-masing).
- Jalankan ketiga project `ApiGateway`, `Payment.API` dan `Transaction.API`.
- Database `transaction.db` akan terbentuk otomatis saat project `Transaction.API` berhasil dijalankan (project ini menggunakan `EntityFramework` dengan alur `Code First` sehingga tidak perlu menjalankan query manual untuk pembentukan database).
- Jika seluruh project berhasil dijalakan, silahkan buka Web Browser anda dan masukkan URL `http://localhost:5000/swagger` untuk membuka dokumentasi API (project ini menggunakan Swagger). Untuk memilih service mana yang ingin dilihat, silahkan ganti di pojok kanan atas halaman Swagger.
- Selamat mencoba!

## Informasi
### Spesifikasi
#### REST API
- Setiap services pada project ini menggunakan REST API yang dapat dieksplorasi pada dokumentasi (Swagger), seperti salah satu contoh REST API untuk mendapatkan data `Order` yang telah dibuat sebelumnya bisa diakses di `http://localhost:5000/transaction/api/v1/Orders?pageIndex=0&pageSize=10`
#### Microservices
- Terdapat 2 service terpisah dalam project ini, yaitu: `Transaction Service` dan `Payment Service`. masing-masing dari service ini dapat berkomunikasi melalui `HTTP Client` atau dengan `Message Broker (RabbitMQ)`.
#### Relational Database (SQL Query, 1-1/1-n/n-n)
- Transaksi yang dilakukan saat Crate, Read, Update dan Delete (CRUD) data menggunakan SQL Query dengan menggunakan `EntityFramework` sebagai ORM-nya.
- `Product` dengan `Tag` memiliki relasi `many-to-many` dimana `Product` bisa memiliki banyak `Tag` dan suatu `Tag` bisa digunakan oleh banyak `Product`. untuk mendesain suatu relasi `many-to-many` maka otomatis akan memerlukan relasi `one-to-many`, dalam relasi antara `Product` dengan `Tag` maka dibuat `Product`1-\*`ProductTag`\*-1`Tag` 
- `Order` dengan `OrderProduct` memiliki relasi `one-to-one` dimana setiap `Order` hanya bisa memilih satu `OrderProduct` (tidak langsung direlasikan dengan `Product` karena `Product` hanyalah master data dimana saat pengguna ingin melakukan update terhadap suatu `Product` maka tidak akan berimplikasi dengan `Product` di `Order` yang sebelumnya (contoh kasus, jika saya melakukan pemesanan dalam suatu product dengan harga 1000 dan sudah melakukan pembayaran dan admin meng-update harga product menjadi 2000 maka harga di pesanan saya tetap 1000, karena secara teknikal `Order` tidak memiliki relasi langsung terhadap `Product` melainkan `OrderProduct`.
#### Transactional Function (CRUD)
- Pengguna dapat melakukan Create, Read, Update dan Delete (CRUD) data yang langsung mengupdate ke database 

## Apa yang bisa dikembangkan dari project ini?
#### Service
- Membuat service `Identity` yang bertugas untuk manajemen user dan otentikasi, sehingga aplikasi ini bisa terproteksi menggunakan otentikasi. contoh hanya pengguna dengan `role Admin` yang dapat menambahkan produk.
- Memisahkan antara `Product` dan `Order` menjadi service masing-masing, salah satu benefit yang dapat dinikmati adalah pengguna masih dapat melihat-lihat produk yang tersedia disaat service `Order` sedang mati atau overload.
- Melakuakan `Unit Test` secara menyeluruh pada setiap method, saat ini hanya bagian-bagian yang perlu saja yang sudah dibuatkan `Unit Test`
- Pengembangan logika bisnis yang lebih baik dan detail.