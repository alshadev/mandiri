using Ocelot.DependencyInjection;
using Ocelot.Middleware;
using Serilog;

string environment = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") ?? string.Empty;

var configurationBuilder = new ConfigurationBuilder()
    .SetBasePath(Directory.GetCurrentDirectory())
    .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
    .AddJsonFile($"appsettings.{environment}.json", optional: true, reloadOnChange: true)
    .AddJsonFile("configuration.json", optional: true, reloadOnChange: true)
    .AddJsonFile($"configuration.{environment}.json", optional: true, reloadOnChange: true)
    .AddEnvironmentVariables();

var configuration = configurationBuilder.Build();

Log.Logger = new LoggerConfiguration()
    .MinimumLevel.Verbose()
    .Enrich.WithProperty("ApplicationContext", "apigateway")
    .Enrich.WithProcessName()
    .Enrich.WithProcessId()
    .Enrich.WithFunction("Severity", e => $"{e.Level}")
    .Enrich.FromLogContext()
    .WriteTo.Console()
    .WriteTo.File("Logs/apigateway-.log", rollingInterval: RollingInterval.Day)
    .ReadFrom.Configuration(configuration)
    .CreateLogger();

Log.Information("Configuring web host (apigateway)...");

var builder = WebApplication.CreateBuilder(args);

builder.Host.UseSerilog();

builder.Services.AddOptions();

builder.Services.AddCors(options =>
{
    options
        .AddPolicy("CorsPolicy", builder => builder.SetIsOriginAllowed((host) => true)
        .AllowAnyMethod()
        .AllowAnyHeader()
        .AllowCredentials());
});

builder.Services.AddOcelot(configuration);
builder.Services.AddSwaggerForOcelot(configuration);

builder.Services.AddControllers();

var app = builder.Build();

app.UseCors("CorsPolicy");

if (app.Environment.IsDevelopment())
{
    string downstreamSwaggerEndPointBasePath = configuration.GetValue<string>("DownstreamSwaggerEndPointBasePath");
    app.UseSwaggerForOcelotUI(opt =>
    {
        opt.PathToSwaggerGenerator = "/swagger/docs";
        if (!string.IsNullOrWhiteSpace(downstreamSwaggerEndPointBasePath))
        {
            opt.DownstreamSwaggerEndPointBasePath = downstreamSwaggerEndPointBasePath + "/swagger/docs";
        }
    });
}

app.UseWebSockets();
app.UseOcelot().Wait();

Log.Information("Starting web host (apigateway)...");

app.Run();
