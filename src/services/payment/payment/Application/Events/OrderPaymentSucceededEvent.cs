﻿namespace transaction.api.Application.Events;

public class OrderPaymentSucceededEvent
{
    public Guid OrderId { get; set; }
}
