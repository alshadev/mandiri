﻿namespace Payment.API.Controllers;

[Route("api/v1/[controller]")]
[ApiController]
public class PaymentsController : ControllerBase
{
    private readonly IPublishEndpoint _endpoint;

    public PaymentsController(IPublishEndpoint endpoint)
    {
        _endpoint = endpoint;
    }

    [HttpPost("{orderId}")]
    [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
    public async Task<ActionResult<bool>> PaymentOrder(Guid orderId) 
    {
        await _endpoint.Publish(new OrderPaymentSucceededEvent() { OrderId = orderId });
        return true;
    }
}
