﻿global using Microsoft.AspNetCore.Http;
global using Microsoft.AspNetCore.Mvc;
global using System.Net;
global using MassTransit;
global using transaction.api.Application.Events;
global using Microsoft.OpenApi.Models;
global using Serilog;
global using System.Text.Json.Serialization;