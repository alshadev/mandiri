﻿using Transaction.API.Application.Commands.ProductCommand;
using Transaction.API.Infrastructures.Repositories;

namespace Transaction.Test.CommandHandler;

public class UnitTest_CreateProductCommandHandler
{
    [Fact]
    public async void Test_Create_Valid_Product() 
    {
        var command = new CreateProductCommand() 
        { 
            Code = "Test",
            Name = "Test",
            Description = "Test",
            Price = 1000,
            Tags = ["Test1", "Test2", "Test3"]
        };

        var cancellationToken = new CancellationToken();

        var mockProductRepository = new Mock<IProductRepository>();
        mockProductRepository.Setup(x => x.IsProductExist("Test")).ReturnsAsync(false);
        mockProductRepository.Setup(x => x.GetTagsAsync()).ReturnsAsync([]);
        mockProductRepository.Setup(x => x.UnitOfWork.SaveChangesAsync(cancellationToken)).ReturnsAsync(1);

        var handler = new CreateProductCommandHandler(mockProductRepository.Object);
        var result = await handler.Handle(command, cancellationToken);

        Assert.True(result != default);
    }

    [Fact]
    public async void Test_Create_InValid_Product_Because_Code_Already_Exist()
    {
        var command = new CreateProductCommand()
        {
            Code = "Test",
            Name = "Test",
            Description = "Test",
            Price = 1000,
            Tags = ["Test1", "Test2", "Test3"]
        };

        var cancellationToken = new CancellationToken();

        var mockProductRepository = new Mock<IProductRepository>();
        mockProductRepository.Setup(x => x.IsProductExist("Test")).ReturnsAsync(true);
        mockProductRepository.Setup(x => x.GetTagsAsync()).ReturnsAsync([]);
        mockProductRepository.Setup(x => x.UnitOfWork.SaveChangesAsync(cancellationToken)).ReturnsAsync(1);

        var handler = new CreateProductCommandHandler(mockProductRepository.Object);

        await Assert.ThrowsAsync<Exception>(() => handler.Handle(command, cancellationToken));
    }
}
