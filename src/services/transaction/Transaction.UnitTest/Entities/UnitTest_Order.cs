﻿namespace Transaction.UnitTest.Entities;

public class UnitTest_Order
{
    [Fact]
    public void Test_Create_Order() 
    {
        var mockProduct = new Mock<Product>();

        var order = new Order("address", mockProduct.Object);

        Assert.NotNull(order);
        Assert.NotNull(order.OrderProduct);
        Assert.Equal("address", order.Address);
        Assert.Equal(OrderStatus.Submitted, order.Status);
    }

    [Fact]
    public void Test_Set_Paid()
    {
        var mockOrder = new Mock<Order>();

        Assert.Equal(OrderStatus.Paid, mockOrder.Object.SetPaid());
    }

    [Fact]
    public void Test_Cancel()
    {
        var mockProduct = new Mock<Product>();

        var orderWithSubmittedStatus = new Order("address", mockProduct.Object);
        var orderWithPaidStatus = new Order("address", mockProduct.Object);
        orderWithPaidStatus.SetPaid();

        Assert.Equal(OrderStatus.Cancelled, orderWithSubmittedStatus.Cancel());
        Assert.Throws<Exception>(() => orderWithPaidStatus.Cancel());
    }
}
