﻿namespace Transaction.UnitTest.Entities;

public class UnitTest_Product
{
    [Fact]
    public void Test_Create_Product() 
    {
        var newProduct = new Product("code1", "name1", 10000);

        Assert.NotNull(newProduct);
    }

    [Fact]
    public void Test_Set_Code() 
    {
        var mockProduct = new Mock<Product>();

        Assert.Equal("CODE", mockProduct.Object.SetCode("    code    "));
        Assert.Throws<Exception>(() => mockProduct.Object.SetCode(""));
        Assert.Throws<Exception>(() => mockProduct.Object.SetCode(" "));
        Assert.Throws<Exception>(() => mockProduct.Object.SetCode(null));
        Assert.Throws<Exception>(() => mockProduct.Object.SetCode(new string('c', 21)));
    }

    [Fact]
    public void Test_Set_Name()
    {
        var mockProduct = new Mock<Product>();

        Assert.Equal("name", mockProduct.Object.SetName("    name    "));
        Assert.Throws<Exception>(() => mockProduct.Object.SetName(""));
        Assert.Throws<Exception>(() => mockProduct.Object.SetName(" "));
        Assert.Throws<Exception>(() => mockProduct.Object.SetName(null));
        Assert.Throws<Exception>(() => mockProduct.Object.SetName(new string('n', 101)));
    }

    [Fact]
    public void Test_Set_Description()
    {
        var mockProduct = new Mock<Product>();

        Assert.Equal("desc", mockProduct.Object.SetDescription("    desc    "));
        Assert.Null(Record.Exception(() => mockProduct.Object.SetDescription("")));
        Assert.Null(Record.Exception(() => mockProduct.Object.SetDescription("  ")));
        Assert.Null(Record.Exception(() => mockProduct.Object.SetDescription(null)));
    }

    [Fact]
    public void Test_Set_Price() 
    {
        var mockProduct = new Mock<Product>();

        Assert.Equal(10, mockProduct.Object.SetPrice(10));
        Assert.Throws<Exception>(() => mockProduct.Object.SetPrice(0));
    }

    [Fact]
    public void Test_Add_Tag() 
    {
        var mockProduct = new Mock<Product>();
        var mockTag = new Mock<Tag>();

        var product = mockProduct.Object;
        var tag = mockTag.Object;

        product.AddTag(tag);

        Assert.NotNull(product.ProductTags);
        Assert.True(product.ProductTags.Count != 0);
    }

    [Fact]
    public void Test_Clear_Tag() 
    {
        var mockProduct = new Mock<Product>();
        var mockTag = new Mock<Tag>();

        var product = mockProduct.Object;
        var tag = mockTag.Object;

        product.AddTag(tag);

        product.ClearTag();

        Assert.NotNull(product.ProductTags);
        Assert.True(product.ProductTags.Count == 0);
    }
}
