﻿namespace Transaction.UnitTest.Entities;

public class UnitTest_ProductTag
{
    [Fact]
    public void Test_Create_ProductTag()
    {
        var product = new Mock<Product>();
        var tag = new Mock<Tag>();

        var productTag = new ProductTag(product.Object, tag.Object);

        Assert.NotNull(productTag);
    }
}
