﻿namespace Transaction.UnitTest.Entities;

public class UnitTest_Tag
{
    [Fact]
    public void Test_Create_Tag()
    {
        var tag = new Tag("name1");

        Assert.NotNull(tag);
    }

    [Fact]
    public void Test_Set_Name()
    {
        var mockTag = new Mock<Tag>();

        Assert.Equal("name", mockTag.Object.SetName("    name    "));
        Assert.Throws<Exception>(() => mockTag.Object.SetName(""));
        Assert.Throws<Exception>(() => mockTag.Object.SetName(" "));
        Assert.Throws<Exception>(() => mockTag.Object.SetName(null));
        Assert.Throws<Exception>(() => mockTag.Object.SetName(new string('n', 51)));
    }
}
