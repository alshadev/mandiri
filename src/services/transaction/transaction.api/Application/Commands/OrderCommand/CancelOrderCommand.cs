﻿namespace Transaction.API.Application.Commands.OrderCommand;

public class CancelOrderCommand : IRequest<bool>
{
    public Guid OrderId { get; set; }
}
