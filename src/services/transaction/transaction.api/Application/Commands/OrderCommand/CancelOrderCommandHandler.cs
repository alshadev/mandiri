﻿namespace Transaction.API.Application.Commands.OrderCommand;

public class CancelOrderCommandHandler : IRequestHandler<CancelOrderCommand, bool>
{
    private readonly IOrderRepository _orderRepository;

    public CancelOrderCommandHandler(IOrderRepository orderRepository)
    {
        _orderRepository = orderRepository;
    }

    public async Task<bool> Handle(CancelOrderCommand request, CancellationToken cancellationToken)
    {
        var order = await _orderRepository.GetByIdAsync(request.OrderId);

        order.Cancel();

        //TODO: add event to cancel payment process

        await _orderRepository.UnitOfWork.SaveChangesAsync(cancellationToken);

        return true;
    }
}
