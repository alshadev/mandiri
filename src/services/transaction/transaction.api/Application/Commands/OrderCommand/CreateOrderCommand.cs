﻿namespace Transaction.API.Application.Commands.OrderCommand;

public class CreateOrderCommand : IRequest<Guid>
{
    public string Address { get; set; }
    public Guid ProductId { get; set; }
}
