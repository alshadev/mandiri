﻿namespace Transaction.API.Application.Commands.OrderCommand;

public class CreateOrderCommandHandler : IRequestHandler<CreateOrderCommand, Guid>
{
    private readonly IOrderRepository _orderRepository;
    private readonly IProductRepository _productRepository;

    public CreateOrderCommandHandler(IOrderRepository orderRepository, IProductRepository productRepository)
    {
        _orderRepository = orderRepository;
        _productRepository = productRepository;
    }

    public async Task<Guid> Handle(CreateOrderCommand request, CancellationToken cancellationToken)
    {
        var product = await _productRepository.GetByIdAsync(request.ProductId);

        var order = new Order(request.Address, product);

        //TODO: add event to payment process

        _orderRepository.Add(order);

        await _orderRepository.UnitOfWork.SaveChangesAsync(cancellationToken);

        return order.Id;
    }
}
