﻿namespace Transaction.API.Application.Commands.OrderCommand;

public class UpdateOrderStatusCommand : IRequest<bool>
{
    public Guid OrderId { get; set; }
    public OrderStatus Status { get; set; }
}
