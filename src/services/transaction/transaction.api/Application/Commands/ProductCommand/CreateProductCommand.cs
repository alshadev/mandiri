﻿namespace Transaction.API.Application.Commands.ProductCommand;

public class CreateProductCommand : IRequest<Guid>
{
    public string Code { get; set; }
    public string Name { get; set; }
    public string Description { get; set; }
    public decimal Price { get; set; }
    public List<string> Tags { get; set; }
}
