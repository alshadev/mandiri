﻿namespace Transaction.API.Application.Commands.ProductCommand;

public class CreateProductCommandHandler : IRequestHandler<CreateProductCommand, Guid>
{
    private readonly IProductRepository _productRepository;

    public CreateProductCommandHandler(IProductRepository productRepository)
    {
        _productRepository = productRepository;
    }

    public async Task<Guid> Handle(CreateProductCommand request, CancellationToken cancellationToken)
    {
        if (await _productRepository.IsProductExist(request.Code)) 
        {
            throw new Exception($"product code '{request.Code}' is alredy exist");
        }

        var product = new Product(request.Code, request.Name, request.Price);
        product.SetDescription(request.Description);

        var existingTags = await _productRepository.GetTagsAsync();

        foreach(var requestTag in request.Tags) 
        {
            var existingTag = existingTags.FirstOrDefault(x => x.Name == requestTag);
            product.AddTag(existingTag ?? new Tag(requestTag));
        }

        _productRepository.Add(product);

        await _productRepository.UnitOfWork.SaveChangesAsync(cancellationToken);

        return product.Id;
    }
}
