﻿namespace Transaction.API.Application.Commands.ProductCommand;

public class DeleteProductCommand : IRequest<bool>
{
    public Guid Id { get; set; }
}
