﻿namespace Transaction.API.Application.Commands.ProductCommand;

public class DeleteProductCommandHandler : IRequestHandler<DeleteProductCommand, bool>
{
    private readonly IProductRepository _productRepository;

    public DeleteProductCommandHandler(IProductRepository productRepository)
    {
        _productRepository = productRepository;
    }

    public async Task<bool> Handle(DeleteProductCommand request, CancellationToken cancellationToken)
    {
        await _productRepository.DeleteByIdAsync(request.Id);
        await _productRepository.UnitOfWork.SaveChangesAsync(cancellationToken);

        return true;
    }
}
