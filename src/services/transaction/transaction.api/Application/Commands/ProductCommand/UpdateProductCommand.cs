﻿namespace Transaction.API.Application.Commands.ProductCommand;

public class UpdateProductCommand : IRequest<bool>
{
    public Guid Id { get; set; }
    public string Name { get; set; }
    public string Description { get; set; }
    public decimal Price { get; set; }
    public List<string> Tags { get; set; }
}
