﻿namespace Transaction.API.Application.Commands.ProductCommand;

public class UpdateProductCommandHandler : IRequestHandler<UpdateProductCommand, bool>
{
    private readonly IProductRepository _productRepository;

    public UpdateProductCommandHandler(IProductRepository productRepository)
    {
        _productRepository = productRepository;
    }

    public async Task<bool> Handle(UpdateProductCommand request, CancellationToken cancellationToken)
    {
        var product = await _productRepository.GetByIdAsync(request.Id);

        product.SetName(request.Name);
        product.SetDescription(request.Description);
        product.SetPrice(request.Price);
        product.ClearTag();

        var existingTags = await _productRepository.GetTagsAsync();

        foreach (var requestTag in request.Tags) 
        {
            var existingTag = existingTags.FirstOrDefault(x => x.Name == requestTag);

            if (existingTag == default)
            {
                var newTag = new Tag(requestTag);
                await _productRepository.AddTagAsync(newTag);

                product.AddTag(newTag);
            }
            else 
            {
                product.AddTag(existingTag);
            }
        }

        await _productRepository.UnitOfWork.SaveChangesAsync(cancellationToken);

        return true;
    }
}
