﻿namespace Transaction.API.Application.Events;

public class OrderPaymentSucceededEvent
{
    public Guid OrderId { get; set; }
}
