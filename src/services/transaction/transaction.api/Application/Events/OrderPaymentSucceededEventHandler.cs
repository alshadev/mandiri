﻿namespace Transaction.API.Application.Events;

public class OrderPaymentSucceededEventHandler : IConsumer<OrderPaymentSucceededEvent>
{
    private readonly IMediator _mediator;
    private readonly ILogger<OrderPaymentSucceededEventHandler> _logger;

    public OrderPaymentSucceededEventHandler(IMediator mediator, ILogger<OrderPaymentSucceededEventHandler> logger)
    {
        _mediator = mediator;
        _logger = logger;
    }

    public async Task Consume(ConsumeContext<OrderPaymentSucceededEvent> context)
    {
        var message = context.Message;

        _logger.LogInformation("Handling event OrderPaymentSucceededEvent: {message}", JsonSerializer.Serialize(message));

        await _mediator.Send(new UpdateOrderStatusCommand() { OrderId = message.OrderId, Status = OrderStatus.Paid });

        _logger.LogInformation("Handled event OrderPaymentSucceededEvent: {message}", JsonSerializer.Serialize(message));
    }
}
