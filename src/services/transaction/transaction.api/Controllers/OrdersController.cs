﻿namespace Transaction.API.Controllers;

[Route("api/v1/[controller]")]
[ApiController]
public class OrdersController : ControllerBase
{
    private readonly IOrderRepository _orderRepository;
    private readonly IMediator _mediator;

    public OrdersController(IOrderRepository orderRepository, IMediator mediator)
    {
        _orderRepository = orderRepository;
        _mediator = mediator;
    }

    [HttpGet]
    [ProducesResponseType(typeof(PaginatedModel<OrderDTO>), (int)HttpStatusCode.OK)]
    public async Task<ActionResult> GetProducts([FromQuery] int pageIndex = 0, [FromQuery] int pageSize = 10)
    {
        var orders = await _orderRepository.PaginatedGetsAsync(pageIndex, pageSize);

        var count = await _orderRepository.Entities.LongCountAsync();

        var paginatedModel = new PaginatedModel<OrderDTO>(pageIndex, pageSize, count, orders.Select(x => x.ToDTO()));

        return Ok(paginatedModel);
    }

    [HttpGet("{id}")]
    public async Task<ActionResult> GetProductByIds(Guid id)
    {
        var orders = await _orderRepository.GetByIdAsync(id);

        return Ok(orders.ToDTO());
    }

    [HttpPost]
    [ProducesResponseType(typeof(Guid), (int)HttpStatusCode.OK)]
    public async Task<ActionResult<Guid>> CreateOrder([FromBody] CreateOrderCommand command) => Ok(await _mediator.Send(command));

    [HttpPut("Cancel")]
    [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
    public async Task<ActionResult<bool>> CancelOrder([FromBody] CancelOrderCommand command) => Ok(await _mediator.Send(command));
}
