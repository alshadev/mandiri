﻿namespace Transaction.API.Controllers;

[Route("api/v1/[controller]")]
[ApiController]
public class ProductsController : ControllerBase
{
    private readonly IProductRepository _productRepository;
    private readonly IMediator _mediator;

    public ProductsController(IProductRepository productRepository, IMediator mediator)
    {
        _productRepository = productRepository;
        _mediator = mediator;
    }

    [HttpGet]
    [ProducesResponseType(typeof(PaginatedModel<ProductDTO>), (int)HttpStatusCode.OK)]
    public async Task<ActionResult> GetProducts([FromQuery] int pageIndex = 0, [FromQuery] int pageSize = 10) 
    {
        var products = await _productRepository.PaginatedGetsAsync(pageIndex, pageSize);

        var count = await _productRepository.Entities.LongCountAsync();

        var paginatedModel = new PaginatedModel<ProductDTO>(pageIndex, pageSize, count, products.Select(x => x.ToDTO()));

        return Ok(paginatedModel);
    }

    [HttpGet("{id}")]
    public async Task<ActionResult> GetProductByIds(Guid id)
    {
        var product = await _productRepository.GetByIdAsync(id);

        return Ok(product.ToDTO());
    }

    [HttpPost]
    [ProducesResponseType(typeof(Guid), (int)HttpStatusCode.OK)]
    public async Task<ActionResult<Guid>> CreateProduct([FromBody] CreateProductCommand command) => Ok(await _mediator.Send(command));

    [HttpPut]
    [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
    public async Task<ActionResult<bool>> UpdateProduct([FromBody] UpdateProductCommand command) => Ok(await _mediator.Send(command));

    [HttpDelete("{id}")]
    [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
    public async Task<ActionResult<bool>> DeleteProduct(Guid id) => Ok(await _mediator.Send(new DeleteProductCommand() { Id = id }));

}
