﻿namespace Transaction.API.DTO;

public class OrderDTO
{
    public DateTime Date { get; set; }
    public string Address { get; set; }
    public string Status { get; set; }
    public OrderProductDTO Product { get; set; }
}

public class OrderProductDTO 
{
    public string Code { get; set; }
    public string Name { get; set; }
    public string Description { get; set; }
    public decimal Price { get; set; }
}
