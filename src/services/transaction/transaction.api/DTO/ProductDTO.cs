﻿namespace Transaction.API.DTO;

public class ProductDTO
{
    public Guid Id { get; set; }
    public string Code { get; set; }
    public string Name { get; set; }
    public string Description { get; set; }
    public decimal Price { get; set; }

    public DateTime CreatedDate { get; set; }

    public List<string> Tags { get; set; }
}
