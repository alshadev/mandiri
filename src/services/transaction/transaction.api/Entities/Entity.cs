﻿namespace Transaction.API.Entities;

public abstract class Entity
{
    Guid _Id;

    public virtual Guid Id
    {
        get
        {
            return _Id;
        }
        protected set
        {
            _Id = value;
        }
    }

    public Guid CreatedById { get; private set; }
    public DateTime CreatedDate { get; private set; }
    public string CreatedBy { get; private set; }
    public Guid? LastModifiedById { get; private set; }
    public DateTime? LastModifiedDate { get; private set; }
    public string LastModifiedBy { get; private set; }

    public void SetCreatedById(Guid UserId)
    {
        CreatedById = UserId;
    }

    public void SetCreatedBy(string fullname)
    {
        CreatedBy = fullname;
    }

    public void SetLastModifiedById(Guid UserId)
    {
        LastModifiedById = UserId;
        LastModifiedDate = DateTime.UtcNow;
    }

    public void SetLastModifiedBy(string fullname)
    {
        LastModifiedBy = fullname;
    }

    public Entity()
    {
        Id = Guid.NewGuid();
        CreatedDate = DateTime.UtcNow;
    }
}
