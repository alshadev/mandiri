﻿namespace Transaction.API.Entities;

public class Order : Entity
{
    public DateTime Date { get; private set; }
    public string Address { get; private set; }
    public OrderStatus Status { get; private set; }

    public OrderProduct OrderProduct { get; private set; }

    public Order()
    {
        Date = DateTime.Now;
        Status = OrderStatus.Submitted;
    }

    public Order(string address, Product product)
        : this()
    {
        SetAddress(address);
        SetProduct(product);
    }

    private void SetAddress(string address) => Address = string.IsNullOrWhiteSpace(address) ? throw new Exception("Address is Mandatory") : address;
    private void SetProduct(Product product)
    {
        if (OrderProduct != null)
        {
            throw new Exception("This order already has product, please create new order for different product");
        }

        OrderProduct = new OrderProduct(product.Code, product.Name, product.Description, product.Price, this);
    }

    public OrderStatus SetPaid() => Status = OrderStatus.Paid;
    public OrderStatus Cancel()
    {
        if (Status != OrderStatus.Submitted) 
        {
            throw new Exception($"Unable to cancel since status already '{Status}'");
        }

        return Status = OrderStatus.Cancelled;
    }
}
