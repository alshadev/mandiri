﻿namespace Transaction.API.Entities;

public class OrderProduct : Entity
{
    public string ProductCode { get; private set; }
    public string ProductName { get; private set; }
    public string ProductDescription { get; private set; }
    public decimal ProductPrice { get; private set; }
    public Guid OrderId { get; private set; }
    public Order Order { get; private set; }

    public OrderProduct()
    {
        
    }

    public OrderProduct(string productCode, string productName, string productDescription, decimal productPrice, Order order)
    {
        Update(productCode, productName, productDescription, productPrice);
        Order = order;
        OrderId = order.Id;
    }

    private void Update(string productCode, string productName, string productDescription, decimal productPrice) 
    {
        ProductCode = productCode;
        ProductName = productName;
        ProductDescription = productDescription;
        ProductPrice = productPrice;
    }
}
