﻿namespace Transaction.API.Entities;

public enum OrderStatus
{
    Submitted = 1,
    Paid = 2,
    Cancelled = 3,
}
