﻿namespace Transaction.API.Entities;

public class Product : Entity
{
    public string Code { get; private set; }
    public string Name { get; private set; }
    public string Description { get; private set; }

    public decimal Price { get; private set; }

    public IReadOnlyCollection<ProductTag> ProductTags { get => _productTags; }
    private readonly List<ProductTag> _productTags;

    public Product()
    {
        _productTags = new List<ProductTag>();
    }

    public Product(string code, string name, decimal price)
        : this()
    {
        SetCode(code);
        SetName(name);
        Price = price;
    }

    public string SetCode(string code) 
    {
        code = code?.Trim();

        if (string.IsNullOrWhiteSpace(code)) 
        {
            throw new Exception("Code is mandatory");
        }

        if (code.Length > 20) 
        {
            throw new Exception("Maximum length of Code is 20");
        }

        return Code = code.ToUpperInvariant();
    }

    public string SetName(string name)
    {
        name = name?.Trim();

        if (string.IsNullOrWhiteSpace(name))
        {
            throw new Exception("Name is mandatory");
        }

        if (name.Length > 100)
        {
            throw new Exception("Maximum length of Name is 100");
        }

        return Name = name;
    }

    public string SetDescription(string description) 
    {
        return Description = description?.Trim();
    }

    public decimal SetPrice(decimal price) 
    {
        if (price < 1) 
        { 
            throw new Exception($"Invalid Price");
        }

        return Price = price;
    }

    public void AddTag(Tag tag) 
    {
        var productTag = new ProductTag(this, tag);
        _productTags.Add(productTag);
    }

    public void ClearTag() => _productTags.Clear();
}
