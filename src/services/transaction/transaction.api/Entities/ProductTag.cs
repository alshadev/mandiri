﻿namespace Transaction.API.Entities;

public class ProductTag
{
    public Guid ProductId { get; private set; }
    public Guid TagId { get; private set; }
    public Product Product { get; private set; }
    public Tag Tag { get; private set; }

    public ProductTag()
    {
        
    }

    public ProductTag(Product product, Tag tag)
    {
        Product = product ?? throw new Exception("Product is Mandatory");
        Tag = tag ?? throw new Exception("Tag is Mandatory");
    }
}
