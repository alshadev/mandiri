﻿namespace Transaction.API.Entities;

public class Tag : Entity
{
    public string Name { get; private set; }

    public Tag()
    {
        
    }

    public Tag(string name)
        : this()
    {
        SetName(name);
    }

    public string SetName(string name) 
    {
        name = name?.Trim();

        if (string.IsNullOrWhiteSpace(name))
        {
            throw new Exception("Name is mandatory");
        }

        if (name.Length > 50)
        {
            throw new Exception("Maximum length of Name is 50");
        }

        return Name = name;
    }
}
