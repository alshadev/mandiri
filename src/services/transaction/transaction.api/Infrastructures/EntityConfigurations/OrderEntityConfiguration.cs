﻿namespace Transaction.API.Infrastructures.EntityConfigurations;

public class OrderEntityConfiguration : IEntityTypeConfiguration<Order>
{
    public void Configure(EntityTypeBuilder<Order> builder)
    {
        builder.Property(x => x.Address).IsRequired();

        builder.Property(x => x.Status)
            .HasConversion(x => x.ToString(), x => (OrderStatus)Enum.Parse(typeof(OrderStatus), x))
            .HasMaxLength(20);

        builder
            .HasOne(x => x.OrderProduct)
            .WithOne(x => x.Order)
            .HasForeignKey<OrderProduct>(x => x.OrderId)
            .IsRequired()
            .OnDelete(deleteBehavior: DeleteBehavior.Restrict);

        builder.HasIndex(x => x.Date);
        builder.HasIndex(x => x.Status);
    }
}
