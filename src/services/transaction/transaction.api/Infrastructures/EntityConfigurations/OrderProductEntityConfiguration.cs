﻿namespace Transaction.API.Infrastructures.EntityConfigurations;

public class OrderProductEntityConfiguration : IEntityTypeConfiguration<OrderProduct>
{
    public void Configure(EntityTypeBuilder<OrderProduct> builder)
    {
        builder.Property(x => x.ProductCode).IsRequired().HasMaxLength(20);
        builder.Property(x => x.ProductName).IsRequired().HasMaxLength(100);
    }
}
