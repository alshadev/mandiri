﻿namespace Transaction.API.Infrastructures.Mapper;

public static class OrderMapper
{
    public static OrderDTO ToDTO(this Order entity) 
    {
        return new OrderDTO() 
        { 
            Date = entity.Date,
            Address = entity.Address,
            Status = entity.Status.ToString(),
            Product = new OrderProductDTO() 
            { 
                Code = entity.OrderProduct.ProductCode,
                Name = entity.OrderProduct.ProductName,
                Description = entity.OrderProduct.ProductDescription,
                Price = entity.OrderProduct.ProductPrice,
            }
        };
    }
}
