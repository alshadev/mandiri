﻿namespace Transaction.API.Infrastructures.Mapper;

public static class ProductMapper
{
    public static ProductDTO ToDTO(this Product entity) 
    {
        return new ProductDTO() 
        { 
            Id = entity.Id,
            Code = entity.Code,
            Name = entity.Name,
            Description = entity.Description,
            Price = entity.Price,
            CreatedDate = entity.CreatedDate,
            Tags = entity.ProductTags?.Select(x => x.Tag?.Name).ToList(),
        };
    }
}
