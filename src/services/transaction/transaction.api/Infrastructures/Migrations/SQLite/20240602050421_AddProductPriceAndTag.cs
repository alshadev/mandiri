﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Transaction.API.Infrastructures.Migrations.SQLite
{
    /// <inheritdoc />
    public partial class AddProductPriceAndTag : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<decimal>(
                name: "Price",
                schema: "Transaction",
                table: "Products",
                type: "decimal(28,10)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.CreateTable(
                name: "Tags",
                schema: "Transaction",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "TEXT", nullable: false),
                    Name = table.Column<string>(type: "TEXT", maxLength: 50, nullable: false),
                    CreatedById = table.Column<Guid>(type: "TEXT", nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "TEXT", nullable: false),
                    CreatedBy = table.Column<string>(type: "TEXT", maxLength: 100, nullable: false),
                    LastModifiedById = table.Column<Guid>(type: "TEXT", nullable: true),
                    LastModifiedDate = table.Column<DateTime>(type: "TEXT", nullable: true),
                    LastModifiedBy = table.Column<string>(type: "TEXT", maxLength: 100, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Tags", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ProductTags",
                schema: "Transaction",
                columns: table => new
                {
                    ProductId = table.Column<Guid>(type: "TEXT", nullable: false),
                    TagId = table.Column<Guid>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductTags", x => new { x.ProductId, x.TagId });
                    table.ForeignKey(
                        name: "FK_ProductTags_Products_ProductId",
                        column: x => x.ProductId,
                        principalSchema: "Transaction",
                        principalTable: "Products",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ProductTags_Tags_TagId",
                        column: x => x.TagId,
                        principalSchema: "Transaction",
                        principalTable: "Tags",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ProductTags_TagId",
                schema: "Transaction",
                table: "ProductTags",
                column: "TagId");

            migrationBuilder.CreateIndex(
                name: "IX_Tags_CreatedBy",
                schema: "Transaction",
                table: "Tags",
                column: "CreatedBy");

            migrationBuilder.CreateIndex(
                name: "IX_Tags_CreatedById",
                schema: "Transaction",
                table: "Tags",
                column: "CreatedById");

            migrationBuilder.CreateIndex(
                name: "IX_Tags_CreatedDate",
                schema: "Transaction",
                table: "Tags",
                column: "CreatedDate");

            migrationBuilder.CreateIndex(
                name: "IX_Tags_LastModifiedBy",
                schema: "Transaction",
                table: "Tags",
                column: "LastModifiedBy");

            migrationBuilder.CreateIndex(
                name: "IX_Tags_LastModifiedById",
                schema: "Transaction",
                table: "Tags",
                column: "LastModifiedById");

            migrationBuilder.CreateIndex(
                name: "IX_Tags_LastModifiedDate",
                schema: "Transaction",
                table: "Tags",
                column: "LastModifiedDate");

            migrationBuilder.CreateIndex(
                name: "IX_Tags_Name",
                schema: "Transaction",
                table: "Tags",
                column: "Name",
                unique: true);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ProductTags",
                schema: "Transaction");

            migrationBuilder.DropTable(
                name: "Tags",
                schema: "Transaction");

            migrationBuilder.DropColumn(
                name: "Price",
                schema: "Transaction",
                table: "Products");
        }
    }
}
