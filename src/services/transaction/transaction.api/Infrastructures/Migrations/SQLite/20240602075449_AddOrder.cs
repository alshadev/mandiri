﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Transaction.API.Infrastructures.Migrations.SQLite
{
    /// <inheritdoc />
    public partial class AddOrder : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Orders",
                schema: "Transaction",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "TEXT", nullable: false),
                    Date = table.Column<DateTime>(type: "TEXT", nullable: false),
                    Address = table.Column<string>(type: "TEXT", nullable: false),
                    Status = table.Column<string>(type: "TEXT", maxLength: 20, nullable: false),
                    CreatedById = table.Column<Guid>(type: "TEXT", nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "TEXT", nullable: false),
                    CreatedBy = table.Column<string>(type: "TEXT", maxLength: 100, nullable: false),
                    LastModifiedById = table.Column<Guid>(type: "TEXT", nullable: true),
                    LastModifiedDate = table.Column<DateTime>(type: "TEXT", nullable: true),
                    LastModifiedBy = table.Column<string>(type: "TEXT", maxLength: 100, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Orders", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "OrderProducts",
                schema: "Transaction",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "TEXT", nullable: false),
                    ProductCode = table.Column<string>(type: "TEXT", maxLength: 20, nullable: false),
                    ProductName = table.Column<string>(type: "TEXT", maxLength: 100, nullable: false),
                    ProductDescription = table.Column<string>(type: "TEXT", nullable: true),
                    ProductPrice = table.Column<decimal>(type: "decimal(28,10)", nullable: false),
                    OrderId = table.Column<Guid>(type: "TEXT", nullable: false),
                    CreatedById = table.Column<Guid>(type: "TEXT", nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "TEXT", nullable: false),
                    CreatedBy = table.Column<string>(type: "TEXT", maxLength: 100, nullable: false),
                    LastModifiedById = table.Column<Guid>(type: "TEXT", nullable: true),
                    LastModifiedDate = table.Column<DateTime>(type: "TEXT", nullable: true),
                    LastModifiedBy = table.Column<string>(type: "TEXT", maxLength: 100, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OrderProducts", x => x.Id);
                    table.ForeignKey(
                        name: "FK_OrderProducts_Orders_OrderId",
                        column: x => x.OrderId,
                        principalSchema: "Transaction",
                        principalTable: "Orders",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_OrderProducts_CreatedBy",
                schema: "Transaction",
                table: "OrderProducts",
                column: "CreatedBy");

            migrationBuilder.CreateIndex(
                name: "IX_OrderProducts_CreatedById",
                schema: "Transaction",
                table: "OrderProducts",
                column: "CreatedById");

            migrationBuilder.CreateIndex(
                name: "IX_OrderProducts_CreatedDate",
                schema: "Transaction",
                table: "OrderProducts",
                column: "CreatedDate");

            migrationBuilder.CreateIndex(
                name: "IX_OrderProducts_LastModifiedBy",
                schema: "Transaction",
                table: "OrderProducts",
                column: "LastModifiedBy");

            migrationBuilder.CreateIndex(
                name: "IX_OrderProducts_LastModifiedById",
                schema: "Transaction",
                table: "OrderProducts",
                column: "LastModifiedById");

            migrationBuilder.CreateIndex(
                name: "IX_OrderProducts_LastModifiedDate",
                schema: "Transaction",
                table: "OrderProducts",
                column: "LastModifiedDate");

            migrationBuilder.CreateIndex(
                name: "IX_OrderProducts_OrderId",
                schema: "Transaction",
                table: "OrderProducts",
                column: "OrderId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Orders_CreatedBy",
                schema: "Transaction",
                table: "Orders",
                column: "CreatedBy");

            migrationBuilder.CreateIndex(
                name: "IX_Orders_CreatedById",
                schema: "Transaction",
                table: "Orders",
                column: "CreatedById");

            migrationBuilder.CreateIndex(
                name: "IX_Orders_CreatedDate",
                schema: "Transaction",
                table: "Orders",
                column: "CreatedDate");

            migrationBuilder.CreateIndex(
                name: "IX_Orders_Date",
                schema: "Transaction",
                table: "Orders",
                column: "Date");

            migrationBuilder.CreateIndex(
                name: "IX_Orders_LastModifiedBy",
                schema: "Transaction",
                table: "Orders",
                column: "LastModifiedBy");

            migrationBuilder.CreateIndex(
                name: "IX_Orders_LastModifiedById",
                schema: "Transaction",
                table: "Orders",
                column: "LastModifiedById");

            migrationBuilder.CreateIndex(
                name: "IX_Orders_LastModifiedDate",
                schema: "Transaction",
                table: "Orders",
                column: "LastModifiedDate");

            migrationBuilder.CreateIndex(
                name: "IX_Orders_Status",
                schema: "Transaction",
                table: "Orders",
                column: "Status");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "OrderProducts",
                schema: "Transaction");

            migrationBuilder.DropTable(
                name: "Orders",
                schema: "Transaction");
        }
    }
}
