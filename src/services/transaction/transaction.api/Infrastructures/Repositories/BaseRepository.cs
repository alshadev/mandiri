﻿namespace Transaction.API.Infrastructures.Repositories;

public interface IBaseRepository<T> where T : Entity
{    
    IUnitOfWork UnitOfWork { get; }
    IQueryable<T> Entities { get; }

    void Add(T entity);
    Task AddRangeAsync(params T[] entities);
    void Remove(T entity);
    void RemoveRange(params T[] entities);
}

public abstract class BaseRepository<T, TEntity>(T context)
    where T : DbContext, IUnitOfWork
    where TEntity : class
{
    protected readonly T _context = context;

    public IUnitOfWork UnitOfWork => _context;
    public IQueryable<TEntity> Entities => _context.Set<TEntity>();

    public void Add(TEntity entity) => _context.Add(entity);
    public void Remove(TEntity entity) => _context.Remove(entity);
    public Task AddRangeAsync(params TEntity[] entities) => _context.AddRangeAsync(entities);
    public void RemoveRange(params TEntity[] entities) => _context.RemoveRange(entities);
}