﻿namespace Transaction.API.Infrastructures.Repositories;

public interface IOrderRepository : IBaseRepository<Order> 
{
    Task<IEnumerable<Order>> GetsAsync();
    Task<IEnumerable<Order>> PaginatedGetsAsync(int pageIndex, int pageSize = 10);
    Task<Order> GetByIdAsync(Guid id);
}

public class OrderRepository(TransactionDbContext context) : BaseRepository<TransactionDbContext, Order>(context), IOrderRepository
{
    public async Task<IEnumerable<Order>> GetsAsync()
    {
        var orders = await Entities
            .Include(x => x.OrderProduct)
            .OrderBy(x => x.Date)
            .ToListAsync();

        return orders;
    }

    public async Task<IEnumerable<Order>> PaginatedGetsAsync(int pageIndex = 0, int pageSize = 10)
    {
        var orders = await Entities
            .Include(x => x.OrderProduct)
            .OrderBy(x => x.Date)
            .Skip(pageSize * pageIndex)
            .Take(pageSize)
            .ToListAsync();

        return orders;
    }

    public async Task<Order> GetByIdAsync(Guid id)
    {
        var order = await Entities
            .Include(x => x.OrderProduct)
            .FirstOrDefaultAsync(x => x.Id == id)
            ?? throw new Exception("Order by Id not found");

        return order;
    }
}
