﻿namespace Transaction.API.Infrastructures.Repositories;

public interface IProductRepository : IBaseRepository<Product>
{
    Task<Product> GetByIdAsync(Guid id);
    Task<IEnumerable<Product>> GetsAsync();
    Task<IEnumerable<Product>> PaginatedGetsAsync(int pageIndex, int pageSize = 10);
    Task<bool> DeleteByIdAsync(Guid id);
    Task<bool> IsProductExist(string code);
    Task<bool> AddTagAsync(Tag tag);
    Task<IEnumerable<Tag>> GetTagsAsync();
}

public class ProductRepository(TransactionDbContext context) : BaseRepository<TransactionDbContext, Product>(context), IProductRepository
{
    public async Task<IEnumerable<Product>> GetsAsync() 
    { 
        var products = await Entities
            .Include(x => x.ProductTags)
            .ThenInclude(x => x.Tag)
            .OrderBy(x => x.Code)
            .ToListAsync();

        return products;
    }

    public async Task<IEnumerable<Product>> PaginatedGetsAsync(int pageIndex = 0, int pageSize = 10)
    {
        var products = await Entities
            .Include(x => x.ProductTags)
            .ThenInclude(x => x.Tag)
            .OrderBy(x => x.Code)
            .Skip(pageSize * pageIndex)
            .Take(pageSize)
            .ToListAsync();

        return products;
    }

    public async Task<Product> GetByIdAsync(Guid id) 
    { 
        var product = await Entities
            .Include(x => x.ProductTags)
            .ThenInclude(x => x.Tag)
            .FirstOrDefaultAsync(x => x.Id == id)
            ?? throw new Exception("Product by Id not found");

        return product;
    }

    public async Task<bool> DeleteByIdAsync(Guid id) 
    { 
        var product = await GetByIdAsync(id);
        Remove(product);

        return true;
    }

    public async Task<bool> IsProductExist(string code)
    {
        return await Entities.AnyAsync(x => x.Code == code);
    }

    public async Task<bool> AddTagAsync(Tag tag) 
    { 
        await _context.AddAsync(tag);
        return true;
    }

    public async Task<IEnumerable<Tag>> GetTagsAsync() 
    { 
        var tags = await _context.Tags.ToListAsync();
        return tags;
    }
}
