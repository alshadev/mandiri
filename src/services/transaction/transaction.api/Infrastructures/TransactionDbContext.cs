﻿namespace Transaction.API.Infrastructures;

public interface IUnitOfWork : IDisposable
{
    Task<int> SaveChangesAsync(CancellationToken cancellationToken = default);
}

public class TransactionDbContext : DbContext, IUnitOfWork
{
    public TransactionDbContext(DbContextOptions options) : base(options) 
    { 
    
    }

    public override Task<int> SaveChangesAsync(bool acceptAllChangesOnSuccess, CancellationToken cancellationToken = default)
    {
        Guid userId = default;
        string fullName = "Hardcoded";

        foreach (var entity in this.ChangeTracker.Entries().Where(e => e.State == EntityState.Added))
        {
            if (entity.Entity is Entity)
            {
                ((Entity)entity.Entity).SetCreatedById(userId);
                ((Entity)entity.Entity).SetCreatedBy(fullName);
            }
        }

        foreach (var entity in this.ChangeTracker.Entries().Where(e => e.State == EntityState.Modified))
        {
            if (entity.Entity is Entity)
            {
                ((Entity)entity.Entity).SetLastModifiedById(userId);
                ((Entity)entity.Entity).SetLastModifiedBy(fullName);
            }
        }

        return base.SaveChangesAsync(acceptAllChangesOnSuccess, cancellationToken);
    }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        base.OnConfiguring(optionsBuilder);
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        base.OnModelCreating(modelBuilder);

        modelBuilder.HasDefaultSchema("Transaction");

        modelBuilder.ApplyConfiguration(new ProductEntityConfiguration());
        modelBuilder.ApplyConfiguration(new TagEntityConfiguration());
        modelBuilder.ApplyConfiguration(new ProductTagEntityConfiguration());
        modelBuilder.ApplyConfiguration(new OrderEntityConfiguration());
        modelBuilder.ApplyConfiguration(new OrderProductEntityConfiguration());

        foreach (var property in modelBuilder
            .Model
            .GetEntityTypes()
            .SelectMany(t => t.GetProperties())
            .Where(p => p.ClrType == typeof(decimal) || p.ClrType == typeof(decimal?)))
        {
            property.SetColumnType("decimal(28,10)"); // Maximum SQL Server > 2012 is 38 Digit
        }

        foreach (var entityType in modelBuilder.Model.GetEntityTypes())
        {
            if (entityType.ClrType.IsSubclassOf(typeof(Entity)))
            {
                foreach (var entityProperty in entityType
                    .GetProperties()
                    .Where(p => p.Name == "CreatedById"
                    || p.Name == "LastModifiedById"
                    || p.Name == "CreatedDate"
                    || p.Name == "LastModifiedDate"
                    || p.Name == "CreatedBy"
                    || p.Name == "LastModifiedBy"))
                {
                    entityType.AddIndex(entityProperty);

                    if (entityProperty.Name == "CreatedBy")
                    {
                        entityProperty.SetMaxLength(100);
                        entityProperty.IsNullable = false;
                    }

                    if (entityProperty.Name == "LastModifiedBy")
                    {
                        entityProperty.SetMaxLength(100);
                        entityProperty.IsNullable = true;
                    }
                }
            }
        }
    }

    public DbSet<Product> Products { get; set; }
    public DbSet<Tag> Tags { get; set; }
    public DbSet<ProductTag> ProductTags { get; set; }
    public DbSet<Order> Orders { get; set; }
    public DbSet<OrderProduct> OrderProducts { get; set; }
}


