﻿namespace Transaction.API.Infrastructures;

public interface IDbSeeder<in TContext> where TContext : DbContext
{
    Task SeedAsync(TContext context);
}

public class TransactionDbContextSeed : IDbSeeder<TransactionDbContext>
{
    public async Task SeedAsync(TransactionDbContext context)
    {
        if (!await context.Products.AnyAsync()) 
        {
            var weddingTag = new Tag("Wedding");
            var halloweenTag = new Tag("Halloween");

            var productWedding1 = new Product("WD-001", "Pakaian Pernikahan Wanita", 2000000);
            productWedding1.SetDescription("Pakaian pernikahan untuk wanita dengan tema outdoor pantai, cocok dipasangkan dengan product 'WD-002' sebagai pasangan pria");
            productWedding1.AddTag(weddingTag);

            var productWedding2 = new Product("WD-002", "Pakaian Pernikahan Pria", 2000000);
            productWedding2.SetDescription("Pakaian pernikahan untuk pria dengan tema outdoor pantai, cocok dipasangkan dengan product 'WD-001' sebagai pasangan wanita");
            productWedding2.AddTag(weddingTag);

            var productHalloween1 = new Product("HW-001", "Kostum Halloween bertema Vampire", 500000);
            productHalloween1.SetDescription("Pakaian yang cocok digunakan saat acara Halloween, pakaian ini bertema Vampire yang pastinya dapat membuat orang lain takut loh!");
            productHalloween1.AddTag(halloweenTag);

            var products = new List<Product>() 
            { 
                productWedding1, productWedding2, productHalloween1
            };

            await context.AddRangeAsync(products);
        }
        await context.SaveChangesAsync();
    }
}
