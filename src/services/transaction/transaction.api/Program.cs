string environment = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") ?? string.Empty;

var configurationBuilder = new ConfigurationBuilder()
    .SetBasePath(Directory.GetCurrentDirectory())
    .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
    .AddJsonFile($"appsettings.{environment}.json", optional: true, reloadOnChange: true)
    .AddEnvironmentVariables();

var configuration = configurationBuilder.Build();

Log.Logger = new LoggerConfiguration()
    .MinimumLevel.Verbose()
    .Enrich.WithProperty("ApplicationContext", "Transaction.API")
    .Enrich.WithProcessName()
    .Enrich.WithProcessId()
    .Enrich.WithFunction("Severity", e => $"{e.Level}")
    .Enrich.FromLogContext()
    .WriteTo.Console()
    .WriteTo.File("Logs/Transaction.API-.log", rollingInterval: RollingInterval.Day)
    .ReadFrom.Configuration(configuration)
    .CreateLogger();

Log.Information("Configuring web host (Transaction.API)...");

var builder = WebApplication.CreateBuilder(args);

builder.Host.UseSerilog();

builder.Services.AddOptions();

builder.Services.AddControllers().AddJsonOptions(options =>
{
    options.JsonSerializerOptions.WriteIndented = true;
    options.JsonSerializerOptions.ReferenceHandler = ReferenceHandler.IgnoreCycles;
    options.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter());
});

builder.Services.AddCors(options =>
{
    options
        .AddPolicy("CorsPolicy", builder => builder.SetIsOriginAllowed((host) => true)
        .AllowAnyMethod()
        .AllowAnyHeader()
        .AllowCredentials());
});

builder.Services.AddEndpointsApiExplorer();

builder.Services.AddSwaggerGen(options =>
{
    options.SwaggerDoc("v1", new OpenApiInfo
    {
        Title = "Transaction.API",
        Version = "v1",
        Description = "api documentation for transaction service"
    });
});

builder.Services.AddDbContext<TransactionDbContext>(options =>
{
    options.UseSqlite("Data Source=transaction.db", opt =>
    {
        opt.MigrationsAssembly(typeof(TransactionDbContext).Assembly.GetName().Name);
    });

    options.ConfigureWarnings(x => x.Ignore(RelationalEventId.AmbientTransactionWarning));
});

builder.Services.AddScoped<DbContext, TransactionDbContext>();
builder.Services.AddScoped<IDbSeeder<TransactionDbContext>, TransactionDbContextSeed>();

builder.Services.AddHttpContextAccessor();

builder.Services.AddProblemDetails();

#region Repository
builder.Services.AddScoped<IProductRepository, ProductRepository>();
builder.Services.AddScoped<IOrderRepository, OrderRepository>();
#endregion

#region MediatR
builder.Services.AddMediatR(new MediatRServiceConfiguration().RegisterServicesFromAssemblies(typeof(CreateProductCommandHandler).Assembly));
#endregion

builder.Services.AddMassTransit(x =>
{
    x.AddConsumer<OrderPaymentSucceededEventHandler>();

    x.UsingRabbitMq((context, cfg) =>
    {
        cfg.Host(configuration["RabbitMq:Host"], ushort.Parse(configuration["RabbitMq:Port"]), "/", opt =>
        {
            opt.Username(configuration["RabbitMq:Username"]);
            opt.Password(configuration["RabbitMq:Password"]);
        });

        cfg.ConfigureEndpoints(context);
    });
});

var app = builder.Build();

app.UseRouting();

app.UseAuthentication();

app.UseAuthorization();

app.MapDefaultControllerRoute();

app.MapControllers();

if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
    app.MapGet("/", () => Results.Redirect("/swagger")).ExcludeFromDescription();
}

using (var scope = app.Services.CreateScope())
{
    var logger = scope.ServiceProvider.GetRequiredService<ILogger<Program>>();

    try
    {
        Task.Run(async () =>
        {
            var dbContext = scope.ServiceProvider.GetRequiredService<TransactionDbContext>();
            var seed = scope.ServiceProvider.GetRequiredService<IDbSeeder<TransactionDbContext>>();

            logger.LogInformation("Migrating database associated with context {DbContextName}", typeof(TransactionDbContext).Name);

            var strategy = dbContext.Database.CreateExecutionStrategy();

            await strategy.ExecuteAsync(async () => 
            { 
                await dbContext.Database.MigrateAsync();
                await seed.SeedAsync(dbContext);
            });
        }).Wait();

    }
    catch (Exception ex)
    {
        logger.LogError("An error occured while running auto-migration");
        logger.LogError(ex.ToString());
    }
}

Log.Information("Starting web host (Transaction.API)...");

app.Run();
